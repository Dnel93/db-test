package com.example.test_database;

import com.example.test_database.repository.EmployeesJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TestDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(TestDatabaseApplication.class, args);

	}

}
