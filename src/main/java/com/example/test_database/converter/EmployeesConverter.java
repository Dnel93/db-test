package com.example.test_database.converter;

import com.example.test_database.entity.Employees;
import com.example.test_database.model.EmployeesModel;
import org.springframework.stereotype.Component;

@Component("EmployeesConverter")
public class EmployeesConverter {

    // Entity --> Model
    public EmployeesModel entityToModel(Employees employees){
        EmployeesModel employeesModel = new EmployeesModel(employees);
        return employeesModel;
    }

    // Model --> Entity
    public static Employees modelToEntity(EmployeesModel employeesModel) {
        Employees employees = new Employees(employeesModel);
        return employees;

    }

}
