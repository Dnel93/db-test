package com.example.test_database.repository;

import com.example.test_database.entity.Employees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository("EmployeesJpaRepository")
public interface EmployeesJpaRepository extends JpaRepository<Employees, Serializable> {

    @Query(
            value = "SELECT * FROM EMPLOYEES WHERE first_name = :first_name ",
            nativeQuery = true
    )
    public abstract Employees getEmployeeByName(@Param(value = "first_name") String name);
}
