package com.example.test_database.entity;

import com.example.test_database.model.EmployeesModel;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "EMPLOYEES", schema = "SHOP")
public class Employees implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", nullable = false)
    private int id;

    @Column(name = "FIRST_NAME", nullable = false)
    private String first_name;

    @Column(name = "LAST_NAME", nullable = false)
    private String last_name;

    @Column(name="EMAIL", nullable = false)
    private String email;

    public Employees() {
    }

    public Employees(EmployeesModel employeesModel) {
        this.first_name = employeesModel.getFirst_name();
        this.last_name = employeesModel.getLast_name();
        this.email = employeesModel.getEmail();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Employees{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
