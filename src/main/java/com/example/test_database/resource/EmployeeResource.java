package com.example.test_database.resource;

import com.example.test_database.converter.EmployeesConverter;
import com.example.test_database.entity.Employees;
import com.example.test_database.model.EmployeesModel;
import com.example.test_database.service.EmployeesService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(path = "/Employee")
public class EmployeeResource {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    @Qualifier("EmployeesServiceImpl")
    private EmployeesService employeesService;

    @GetMapping(path = "/list", produces = "application/json")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public List<Employees> listAllEmployees() {
        LOGGER.info("Getting list of Employees");
        return employeesService.listOfEmployees();
    }

    @GetMapping(path = "/{name}", produces = "application/json")
    @ResponseStatus(code = HttpStatus.ACCEPTED)
    public Employees getEmployeeByName(@PathVariable(value = "name") String name){
        LOGGER.info("Getting information for user " + name);
        return employeesService.getEmployeeByName(name);
    }

    @PostMapping(path = "/addEmployee", consumes = "application/json", produces = "application/json")
    @ResponseStatus(code = HttpStatus.CREATED)
    public Employees addEmployee(@RequestBody EmployeesModel employeesModel) {
        LOGGER.info("Adding employee");
        return employeesService.addEmployee(EmployeesConverter.modelToEntity(employeesModel));
    }
}
