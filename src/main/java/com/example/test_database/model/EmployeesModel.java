package com.example.test_database.model;

import com.example.test_database.entity.Employees;

public class EmployeesModel {
    private int id;
    private String first_name;
    private String last_name;
    private String email;

    public EmployeesModel() {
    }

    public EmployeesModel(Employees employees) {
        this.first_name = employees.getFirst_name();
        this.last_name = employees.getLast_name();
        this.email = employees.getEmail();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
