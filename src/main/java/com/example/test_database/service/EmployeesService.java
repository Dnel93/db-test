package com.example.test_database.service;

import com.example.test_database.entity.Employees;

import java.util.List;

public interface EmployeesService {
    public abstract List<Employees> listOfEmployees();

    public abstract Employees addEmployee(Employees employee);

    public abstract Employees getEmployeeByName(String name);

    public abstract void deleteEmployeeById(int employeeId);
}
