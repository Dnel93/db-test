package com.example.test_database.service;

import com.example.test_database.entity.Employees;
import com.example.test_database.repository.EmployeesJpaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("EmployeesServiceImpl")
public class EmployeesServiceImpl implements EmployeesService {

    @Autowired
    @Qualifier("EmployeesJpaRepository")
    EmployeesJpaRepository employeesJpaRepository;

    @Override
    public List<Employees> listOfEmployees() {
        return employeesJpaRepository.findAll();
    }

    @Override
    public Employees addEmployee(Employees employee) {
        return employeesJpaRepository.save(employee);
    }

    @Override
    public Employees getEmployeeByName(String name) {
        return employeesJpaRepository.getEmployeeByName(name);
    }

    @Override
    public void deleteEmployeeById(int employeeId) {
        employeesJpaRepository.deleteById(employeeId);
    }
}
